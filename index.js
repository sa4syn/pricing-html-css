checkbox = document.getElementById('slider');

checkbox.addEventListener('change', state => {
    if (state.target.checked) {
        document.getElementById("basic-price").innerHTML = "$199.99";
        document.getElementById("professional-price").innerHTML = "$249.99";
        document.getElementById("master-price").innerHTML = "$399.99";
    } else {
        document.getElementById("basic-price").innerHTML = "$19.99";
        document.getElementById("professional-price").innerHTML = "$24.99";
        document.getElementById("master-price").innerHTML = "$39.99";
    }

});